/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.bean;

import com.uniquindio.sistema.entities.Programa;
import com.uniquindio.sistema.entities.Usuario;
import com.uniquindio.sistema.servicios.IfaceFacultad;
import java.io.Serializable;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Juliancho
 */
@ManagedBean(name="facultadBean")
@ViewScoped
public class BeanFacultad implements Serializable{
  
    private IfaceFacultad facultadService;
    private String idfacultad;
    private String nombre;
    private Collection<Programa> programaCollection;
    private Collection<Usuario> usuarioCollection;

   
    public String getIdfacultad() {
        return idfacultad;
    }

    public void setIdfacultad(String idfacultad) {
        this.idfacultad = idfacultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Collection<Programa> getProgramaCollection() {
        return programaCollection;
    }

    public void setProgramaCollection(Collection<Programa> programaCollection) {
        this.programaCollection = programaCollection;
    }

    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    
    public IfaceFacultad getFacultadService() {
        return facultadService;
    }

    public void setFacultadService(IfaceFacultad facultadService) {
        this.facultadService = facultadService;
    }
    
    
}
