/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.bean;

import com.uniquindio.sistema.entities.Grupoinvestigacion;
import com.uniquindio.sistema.entities.Programa;
import com.uniquindio.sistema.entities.Proyecto;
import com.uniquindio.sistema.entities.Titulo;
import com.uniquindio.sistema.entities.Usuario;
import com.uniquindio.sistema.servicios.IfaceInvestigador;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Juliancho
 */
@ManagedBean(name="investigadorBean")
@ViewScoped
public class BeanInvestigador implements Serializable{
    
    private IfaceInvestigador investigadorService;
    private String idinvestigador;
    private String nombre;
    private String apellidos;
    private BigInteger edad;
    private String telefonofijo;
    private String telefonocelular;
    private String correo;
    private String direccion;
    private Character principal;
    private String rol;
    private String tipovinculacion;
    private String curriculumruta;
    private Collection<Proyecto> proyectoCollection;
    private Grupoinvestigacion idgrupoinv;
    private Programa idprograma;
    private Collection<Usuario> usuarioCollection;
    private Collection<Titulo> tituloCollection;

    public String getIdinvestigador() {
        return idinvestigador;
    }

    public void setIdinvestigador(String idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public BigInteger getEdad() {
        return edad;
    }

    public void setEdad(BigInteger edad) {
        this.edad = edad;
    }

    public String getTelefonofijo() {
        return telefonofijo;
    }

    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }

    public String getTelefonocelular() {
        return telefonocelular;
    }

    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getTipovinculacion() {
        return tipovinculacion;
    }

    public void setTipovinculacion(String tipovinculacion) {
        this.tipovinculacion = tipovinculacion;
    }

    public String getCurriculumruta() {
        return curriculumruta;
    }

    public void setCurriculumruta(String curriculumruta) {
        this.curriculumruta = curriculumruta;
    }

    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    public Grupoinvestigacion getIdgrupoinv() {
        return idgrupoinv;
    }

    public void setIdgrupoinv(Grupoinvestigacion idgrupoinv) {
        this.idgrupoinv = idgrupoinv;
    }

    public Programa getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(Programa idprograma) {
        this.idprograma = idprograma;
    }

    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    public Collection<Titulo> getTituloCollection() {
        return tituloCollection;
    }

    public void setTituloCollection(Collection<Titulo> tituloCollection) {
        this.tituloCollection = tituloCollection;
    }

    
    public IfaceInvestigador getInvestigadorService() {
        return investigadorService;
    }

    public void setInvestigadorService(IfaceInvestigador investigadorService) {
        this.investigadorService = investigadorService;
    }
    
    
}
