/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.bean;


import com.uniquindio.sistema.entities.Facultad;
import com.uniquindio.sistema.entities.Investigador;
import com.uniquindio.sistema.entities.Programa;
import com.uniquindio.sistema.exception.SVException;
import com.uniquindio.sistema.servicios.IfaceLogin;
import com.uniquindio.sistema.util.Utilidades;
import java.io.Serializable;


/**
 *
 * @author Juliancho
 */

public class BeanLogin implements Serializable {
  
    private IfaceLogin loginService;
    private String idUsuario;
    private String clave;
    private String url;
    private String rol;
    private Facultad idfacultad;
    private Investigador idinvestigador;
    private Programa idprograma;
    private String mensaje;
    private boolean status;
   

    public String validarUsuario(){
        try{
            setClave(Utilidades.encriptPassword(getClave()));
            status=false;
            loginService.validarUsuario(this);
            if(this.status){
                Utilidades.addMsgWarn("Mensaje:", getMensaje());
                return url;    
            }else{
               Utilidades.addMsgWarn("Mensaje: error", getMensaje());
               return "login";
            }
            
        }catch(SVException ex){
            Utilidades.addMsgError("Error en validarLogin", ex.getMsgException());
        }
        return "login";
    }

    public IfaceLogin getLoginService() {
        return loginService;
    }

    public void setLoginService(IfaceLogin loginService) {
        this.loginService = loginService;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Facultad getIdfacultad() {
        return idfacultad;
    }

    public void setIdfacultad(Facultad idfacultad) {
        this.idfacultad = idfacultad;
    }

    public Investigador getIdinvestigador() {
        return idinvestigador;
    }

    public void setIdinvestigador(Investigador idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    public Programa getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(Programa idprograma) {
        this.idprograma = idprograma;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
   
    
    
}
