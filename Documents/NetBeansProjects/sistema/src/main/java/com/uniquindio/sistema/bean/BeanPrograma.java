/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.bean;

import com.uniquindio.sistema.entities.Estudiantes;
import com.uniquindio.sistema.entities.Facultad;
import com.uniquindio.sistema.entities.Grupoinvestigacion;
import com.uniquindio.sistema.entities.Investigador;
import com.uniquindio.sistema.entities.Usuario;
import com.uniquindio.sistema.servicios.IfacePrograma;
import java.io.Serializable;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Juliancho
 */

@ManagedBean(name="programaBean")
@ViewScoped
public class BeanPrograma implements Serializable{
    
    private IfacePrograma programaService;
    private String idprograma;
    private String nombre;
    private Collection<Investigador> investigadorCollection;
    private Collection<Estudiantes> estudiantesCollection;
    private Facultad idfacultad;
    private Collection<Grupoinvestigacion> grupoinvestigacionCollection;
    private Collection<Usuario> usuarioCollection;
    
    
    public IfacePrograma getProgramaService() {
        return programaService;
    }

    public void setProgramaService(IfacePrograma programaService) {
        this.programaService = programaService;
    }
    
}
