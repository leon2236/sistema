/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.dao;


import com.uniquindio.sistema.bean.BeanLogin;
import com.uniquindio.sistema.entities.Usuario;

import com.uniquindio.sistema.exception.SVException;
import com.uniquindio.sistema.negocio.NegocioLogin;
import java.time.Clock;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Juliancho
 */
public class DAOLogin implements NegocioLogin{
    
    @PersistenceContext
    private EntityManager em;//Con esta linea instanciamos el administrador de los entities para poder ejecutar los queries y transacciones a la base de datos.
   
    @Override
    public void validarUsuario(BeanLogin bean) throws SVException{
    
        try{
            System.out.println("llego al dao"+bean.getIdUsuario());
            Query query=em.createNamedQuery("Usuario.findByUsuarioAndPassword");//Aca va el nombre del query que se va a invocar
            query.setParameter("usuario",bean.getIdUsuario());//Aqui va el nombre del primer prametro que pusiste en tu query le anteceden : y su valor
            query.setParameter("password",bean.getClave());//Aqui va el nombre del segundo prametro que pusiste en tu query le anteceden : y su valor
            
            List lista=query.getResultList();//Ejecutamos el query y lo guardamos en una lista.
            
            if(lista.size()>0){
                System.out.println("entro al if"+bean.getIdUsuario());
                Usuario obj= (Usuario)lista.get(0);
                System.out.println("se creo de la database"+obj.getRol());
                if(obj!=null){
                    bean.setUrl(obj.getUrl());
                    bean.setRol(obj.getRol());
                    if(obj.getRol().equals("administrador")){
                        obj.setIdfacultad(null);
                        obj.setIdinvestigador(null);
                        obj.setIdprograma(null);
                     }
                    bean.setIdfacultad(obj.getIdfacultad());
                    bean.setIdinvestigador(obj.getIdinvestigador());
                    bean.setIdprograma(obj.getIdprograma());
                    bean.setMensaje("Usuario Encontrado");
                    bean.setStatus(true);
                }else{
                    bean.setStatus(false);
                    bean.setMensaje("El Usuario no Existe..");
                }
                
            }
          
       
    }catch(Exception ex){
        
        throw new SVException("Error en validarUsurio: " + ex.getMessage(),ex);
        
    }
       
  }
    
    
    
}
