/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "CONVOCATORIA", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Convocatoria.findAll", query = "SELECT c FROM Convocatoria c"),
    @NamedQuery(name = "Convocatoria.findByIdconvocatoria", query = "SELECT c FROM Convocatoria c WHERE c.idconvocatoria = :idconvocatoria"),
    @NamedQuery(name = "Convocatoria.findByNombre", query = "SELECT c FROM Convocatoria c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Convocatoria.findByFechapublicacion", query = "SELECT c FROM Convocatoria c WHERE c.fechapublicacion = :fechapublicacion"),
    @NamedQuery(name = "Convocatoria.findByNumeroresolucion", query = "SELECT c FROM Convocatoria c WHERE c.numeroresolucion = :numeroresolucion"),
    @NamedQuery(name = "Convocatoria.findByDescripcionruta", query = "SELECT c FROM Convocatoria c WHERE c.descripcionruta = :descripcionruta")})
public class Convocatoria implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDCONVOCATORIA")
    private String idconvocatoria;
    @Size(max = 400)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "FECHAPUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechapublicacion;
    @Column(name = "NUMERORESOLUCION")
    private BigInteger numeroresolucion;
    @Size(max = 100)
    @Column(name = "DESCRIPCIONRUTA")
    private String descripcionruta;
    @JoinTable(name = "PROY_CONVO", joinColumns = {
        @JoinColumn(name = "IDCONVOCATORIA", referencedColumnName = "IDCONVOCATORIA")}, inverseJoinColumns = {
        @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")})
    @ManyToMany
    private Collection<Proyecto> proyectoCollection;

    public Convocatoria() {
    }

    public Convocatoria(String idconvocatoria) {
        this.idconvocatoria = idconvocatoria;
    }

    public String getIdconvocatoria() {
        return idconvocatoria;
    }

    public void setIdconvocatoria(String idconvocatoria) {
        this.idconvocatoria = idconvocatoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechapublicacion() {
        return fechapublicacion;
    }

    public void setFechapublicacion(Date fechapublicacion) {
        this.fechapublicacion = fechapublicacion;
    }

    public BigInteger getNumeroresolucion() {
        return numeroresolucion;
    }

    public void setNumeroresolucion(BigInteger numeroresolucion) {
        this.numeroresolucion = numeroresolucion;
    }

    public String getDescripcionruta() {
        return descripcionruta;
    }

    public void setDescripcionruta(String descripcionruta) {
        this.descripcionruta = descripcionruta;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconvocatoria != null ? idconvocatoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Convocatoria)) {
            return false;
        }
        Convocatoria other = (Convocatoria) object;
        if ((this.idconvocatoria == null && other.idconvocatoria != null) || (this.idconvocatoria != null && !this.idconvocatoria.equals(other.idconvocatoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Convocatoria[ idconvocatoria=" + idconvocatoria + " ]";
    }
    
}
