/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "CORRECCION", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Correccion.findAll", query = "SELECT c FROM Correccion c"),
    @NamedQuery(name = "Correccion.findByIdcorreccion", query = "SELECT c FROM Correccion c WHERE c.idcorreccion = :idcorreccion"),
    @NamedQuery(name = "Correccion.findByEmisor", query = "SELECT c FROM Correccion c WHERE c.emisor = :emisor"),
    @NamedQuery(name = "Correccion.findByItem", query = "SELECT c FROM Correccion c WHERE c.item = :item"),
    @NamedQuery(name = "Correccion.findByDescripcion", query = "SELECT c FROM Correccion c WHERE c.descripcion = :descripcion")})
public class Correccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDCORRECCION")
    private String idcorreccion;
    @Size(max = 100)
    @Column(name = "EMISOR")
    private String emisor;
    @Size(max = 100)
    @Column(name = "ITEM")
    private String item;
    @Size(max = 2000)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Correccion() {
    }

    public Correccion(String idcorreccion) {
        this.idcorreccion = idcorreccion;
    }

    public String getIdcorreccion() {
        return idcorreccion;
    }

    public void setIdcorreccion(String idcorreccion) {
        this.idcorreccion = idcorreccion;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcorreccion != null ? idcorreccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Correccion)) {
            return false;
        }
        Correccion other = (Correccion) object;
        if ((this.idcorreccion == null && other.idcorreccion != null) || (this.idcorreccion != null && !this.idcorreccion.equals(other.idcorreccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Correccion[ idcorreccion=" + idcorreccion + " ]";
    }
    
}
