/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "EVAREXTER", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evarexter.findAll", query = "SELECT e FROM Evarexter e"),
    @NamedQuery(name = "Evarexter.findByIdevaluadorexterno", query = "SELECT e FROM Evarexter e WHERE e.idevaluadorexterno = :idevaluadorexterno"),
    @NamedQuery(name = "Evarexter.findByNombre", query = "SELECT e FROM Evarexter e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Evarexter.findByApellido", query = "SELECT e FROM Evarexter e WHERE e.apellido = :apellido"),
    @NamedQuery(name = "Evarexter.findByCelular", query = "SELECT e FROM Evarexter e WHERE e.celular = :celular"),
    @NamedQuery(name = "Evarexter.findByTelefonoresidencia", query = "SELECT e FROM Evarexter e WHERE e.telefonoresidencia = :telefonoresidencia"),
    @NamedQuery(name = "Evarexter.findByDireccionresidenccia", query = "SELECT e FROM Evarexter e WHERE e.direccionresidenccia = :direccionresidenccia"),
    @NamedQuery(name = "Evarexter.findByImpedimento", query = "SELECT e FROM Evarexter e WHERE e.impedimento = :impedimento"),
    @NamedQuery(name = "Evarexter.findByCorreo", query = "SELECT e FROM Evarexter e WHERE e.correo = :correo"),
    @NamedQuery(name = "Evarexter.findByNumerocuentacorriente", query = "SELECT e FROM Evarexter e WHERE e.numerocuentacorriente = :numerocuentacorriente"),
    @NamedQuery(name = "Evarexter.findByNumerocuentaahorros", query = "SELECT e FROM Evarexter e WHERE e.numerocuentaahorros = :numerocuentaahorros"),
    @NamedQuery(name = "Evarexter.findByBanco", query = "SELECT e FROM Evarexter e WHERE e.banco = :banco"),
    @NamedQuery(name = "Evarexter.findByCiudad", query = "SELECT e FROM Evarexter e WHERE e.ciudad = :ciudad"),
    @NamedQuery(name = "Evarexter.findByAreadesempeno", query = "SELECT e FROM Evarexter e WHERE e.areadesempeno = :areadesempeno"),
    @NamedQuery(name = "Evarexter.findByProgramaounidadacademica", query = "SELECT e FROM Evarexter e WHERE e.programaounidadacademica = :programaounidadacademica"),
    @NamedQuery(name = "Evarexter.findByFacultad", query = "SELECT e FROM Evarexter e WHERE e.facultad = :facultad")})
public class Evarexter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDEVALUADOREXTERNO")
    private String idevaluadorexterno;
    @Size(max = 20)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 20)
    @Column(name = "APELLIDO")
    private String apellido;
    @Size(max = 20)
    @Column(name = "CELULAR")
    private String celular;
    @Size(max = 20)
    @Column(name = "TELEFONORESIDENCIA")
    private String telefonoresidencia;
    @Size(max = 50)
    @Column(name = "DIRECCIONRESIDENCCIA")
    private String direccionresidenccia;
    @Size(max = 1000)
    @Column(name = "IMPEDIMENTO")
    private String impedimento;
    @Size(max = 50)
    @Column(name = "CORREO")
    private String correo;
    @Size(max = 30)
    @Column(name = "NUMEROCUENTACORRIENTE")
    private String numerocuentacorriente;
    @Size(max = 30)
    @Column(name = "NUMEROCUENTAAHORROS")
    private String numerocuentaahorros;
    @Size(max = 20)
    @Column(name = "BANCO")
    private String banco;
    @Size(max = 20)
    @Column(name = "CIUDAD")
    private String ciudad;
    @Size(max = 100)
    @Column(name = "AREADESEMPENO")
    private String areadesempeno;
    @Size(max = 100)
    @Column(name = "PROGRAMAOUNIDADACADEMICA")
    private String programaounidadacademica;
    @Size(max = 50)
    @Column(name = "FACULTAD")
    private String facultad;
    @JoinColumn(name = "IDINSTITUCION", referencedColumnName = "IDINSTITUCION")
    @ManyToOne(optional = false)
    private Institucion idinstitucion;
    @OneToMany(mappedBy = "idevaluexter")
    private Collection<Proyecto> proyectoCollection;

    public Evarexter() {
    }

    public Evarexter(String idevaluadorexterno) {
        this.idevaluadorexterno = idevaluadorexterno;
    }

    public String getIdevaluadorexterno() {
        return idevaluadorexterno;
    }

    public void setIdevaluadorexterno(String idevaluadorexterno) {
        this.idevaluadorexterno = idevaluadorexterno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefonoresidencia() {
        return telefonoresidencia;
    }

    public void setTelefonoresidencia(String telefonoresidencia) {
        this.telefonoresidencia = telefonoresidencia;
    }

    public String getDireccionresidenccia() {
        return direccionresidenccia;
    }

    public void setDireccionresidenccia(String direccionresidenccia) {
        this.direccionresidenccia = direccionresidenccia;
    }

    public String getImpedimento() {
        return impedimento;
    }

    public void setImpedimento(String impedimento) {
        this.impedimento = impedimento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNumerocuentacorriente() {
        return numerocuentacorriente;
    }

    public void setNumerocuentacorriente(String numerocuentacorriente) {
        this.numerocuentacorriente = numerocuentacorriente;
    }

    public String getNumerocuentaahorros() {
        return numerocuentaahorros;
    }

    public void setNumerocuentaahorros(String numerocuentaahorros) {
        this.numerocuentaahorros = numerocuentaahorros;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getAreadesempeno() {
        return areadesempeno;
    }

    public void setAreadesempeno(String areadesempeno) {
        this.areadesempeno = areadesempeno;
    }

    public String getProgramaounidadacademica() {
        return programaounidadacademica;
    }

    public void setProgramaounidadacademica(String programaounidadacademica) {
        this.programaounidadacademica = programaounidadacademica;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public Institucion getIdinstitucion() {
        return idinstitucion;
    }

    public void setIdinstitucion(Institucion idinstitucion) {
        this.idinstitucion = idinstitucion;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idevaluadorexterno != null ? idevaluadorexterno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evarexter)) {
            return false;
        }
        Evarexter other = (Evarexter) object;
        if ((this.idevaluadorexterno == null && other.idevaluadorexterno != null) || (this.idevaluadorexterno != null && !this.idevaluadorexterno.equals(other.idevaluadorexterno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Evarexter[ idevaluadorexterno=" + idevaluadorexterno + " ]";
    }
    
}
