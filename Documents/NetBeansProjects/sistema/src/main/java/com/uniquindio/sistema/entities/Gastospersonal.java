/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "GASTOSPERSONAL", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gastospersonal.findAll", query = "SELECT g FROM Gastospersonal g"),
    @NamedQuery(name = "Gastospersonal.findByIdgasdoce", query = "SELECT g FROM Gastospersonal g WHERE g.idgasdoce = :idgasdoce"),
    @NamedQuery(name = "Gastospersonal.findByNombre", query = "SELECT g FROM Gastospersonal g WHERE g.nombre = :nombre"),
    @NamedQuery(name = "Gastospersonal.findByApellido", query = "SELECT g FROM Gastospersonal g WHERE g.apellido = :apellido"),
    @NamedQuery(name = "Gastospersonal.findByIdinvestigador", query = "SELECT g FROM Gastospersonal g WHERE g.idinvestigador = :idinvestigador"),
    @NamedQuery(name = "Gastospersonal.findByTitulopregrado", query = "SELECT g FROM Gastospersonal g WHERE g.titulopregrado = :titulopregrado"),
    @NamedQuery(name = "Gastospersonal.findByTitulopostgrado", query = "SELECT g FROM Gastospersonal g WHERE g.titulopostgrado = :titulopostgrado"),
    @NamedQuery(name = "Gastospersonal.findByFuncionenproyecto", query = "SELECT g FROM Gastospersonal g WHERE g.funcionenproyecto = :funcionenproyecto"),
    @NamedQuery(name = "Gastospersonal.findByDedicacionhorassemana", query = "SELECT g FROM Gastospersonal g WHERE g.dedicacionhorassemana = :dedicacionhorassemana"),
    @NamedQuery(name = "Gastospersonal.findByNumeromeses", query = "SELECT g FROM Gastospersonal g WHERE g.numeromeses = :numeromeses"),
    @NamedQuery(name = "Gastospersonal.findByValor", query = "SELECT g FROM Gastospersonal g WHERE g.valor = :valor"),
    @NamedQuery(name = "Gastospersonal.findByDocente", query = "SELECT g FROM Gastospersonal g WHERE g.docente = :docente"),
    @NamedQuery(name = "Gastospersonal.findByAuxiliar", query = "SELECT g FROM Gastospersonal g WHERE g.auxiliar = :auxiliar"),
    @NamedQuery(name = "Gastospersonal.findByTecnico", query = "SELECT g FROM Gastospersonal g WHERE g.tecnico = :tecnico"),
    @NamedQuery(name = "Gastospersonal.findByRubro1", query = "SELECT g FROM Gastospersonal g WHERE g.rubro1 = :rubro1"),
    @NamedQuery(name = "Gastospersonal.findByRubro2", query = "SELECT g FROM Gastospersonal g WHERE g.rubro2 = :rubro2"),
    @NamedQuery(name = "Gastospersonal.findByRubro3", query = "SELECT g FROM Gastospersonal g WHERE g.rubro3 = :rubro3"),
    @NamedQuery(name = "Gastospersonal.findByRubro4", query = "SELECT g FROM Gastospersonal g WHERE g.rubro4 = :rubro4"),
    @NamedQuery(name = "Gastospersonal.findByIdestudiante", query = "SELECT g FROM Gastospersonal g WHERE g.idestudiante = :idestudiante")})
public class Gastospersonal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDGASDOCE")
    private String idgasdoce;
    @Size(max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 100)
    @Column(name = "APELLIDO")
    private String apellido;
    @Size(max = 10)
    @Column(name = "IDINVESTIGADOR")
    private String idinvestigador;
    @Size(max = 200)
    @Column(name = "TITULOPREGRADO")
    private String titulopregrado;
    @Size(max = 200)
    @Column(name = "TITULOPOSTGRADO")
    private String titulopostgrado;
    @Size(max = 100)
    @Column(name = "FUNCIONENPROYECTO")
    private String funcionenproyecto;
    @Column(name = "DEDICACIONHORASSEMANA")
    private BigInteger dedicacionhorassemana;
    @Column(name = "NUMEROMESES")
    private BigInteger numeromeses;
    @Column(name = "VALOR")
    private BigInteger valor;
    @Column(name = "DOCENTE")
    private Character docente;
    @Column(name = "AUXILIAR")
    private Character auxiliar;
    @Column(name = "TECNICO")
    private Character tecnico;
    @Column(name = "RUBRO1")
    private BigInteger rubro1;
    @Column(name = "RUBRO2")
    private BigInteger rubro2;
    @Column(name = "RUBRO3")
    private BigInteger rubro3;
    @Column(name = "RUBRO4")
    private BigInteger rubro4;
    @Size(max = 10)
    @Column(name = "IDESTUDIANTE")
    private String idestudiante;
    @JoinColumn(name = "IDPRESUPUESTO", referencedColumnName = "IDPRESUPUESTO")
    @ManyToOne
    private Presupuesto idpresupuesto;

    public Gastospersonal() {
    }

    public Gastospersonal(String idgasdoce) {
        this.idgasdoce = idgasdoce;
    }

    public String getIdgasdoce() {
        return idgasdoce;
    }

    public void setIdgasdoce(String idgasdoce) {
        this.idgasdoce = idgasdoce;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getIdinvestigador() {
        return idinvestigador;
    }

    public void setIdinvestigador(String idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    public String getTitulopregrado() {
        return titulopregrado;
    }

    public void setTitulopregrado(String titulopregrado) {
        this.titulopregrado = titulopregrado;
    }

    public String getTitulopostgrado() {
        return titulopostgrado;
    }

    public void setTitulopostgrado(String titulopostgrado) {
        this.titulopostgrado = titulopostgrado;
    }

    public String getFuncionenproyecto() {
        return funcionenproyecto;
    }

    public void setFuncionenproyecto(String funcionenproyecto) {
        this.funcionenproyecto = funcionenproyecto;
    }

    public BigInteger getDedicacionhorassemana() {
        return dedicacionhorassemana;
    }

    public void setDedicacionhorassemana(BigInteger dedicacionhorassemana) {
        this.dedicacionhorassemana = dedicacionhorassemana;
    }

    public BigInteger getNumeromeses() {
        return numeromeses;
    }

    public void setNumeromeses(BigInteger numeromeses) {
        this.numeromeses = numeromeses;
    }

    public BigInteger getValor() {
        return valor;
    }

    public void setValor(BigInteger valor) {
        this.valor = valor;
    }

    public Character getDocente() {
        return docente;
    }

    public void setDocente(Character docente) {
        this.docente = docente;
    }

    public Character getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(Character auxiliar) {
        this.auxiliar = auxiliar;
    }

    public Character getTecnico() {
        return tecnico;
    }

    public void setTecnico(Character tecnico) {
        this.tecnico = tecnico;
    }

    public BigInteger getRubro1() {
        return rubro1;
    }

    public void setRubro1(BigInteger rubro1) {
        this.rubro1 = rubro1;
    }

    public BigInteger getRubro2() {
        return rubro2;
    }

    public void setRubro2(BigInteger rubro2) {
        this.rubro2 = rubro2;
    }

    public BigInteger getRubro3() {
        return rubro3;
    }

    public void setRubro3(BigInteger rubro3) {
        this.rubro3 = rubro3;
    }

    public BigInteger getRubro4() {
        return rubro4;
    }

    public void setRubro4(BigInteger rubro4) {
        this.rubro4 = rubro4;
    }

    public String getIdestudiante() {
        return idestudiante;
    }

    public void setIdestudiante(String idestudiante) {
        this.idestudiante = idestudiante;
    }

    public Presupuesto getIdpresupuesto() {
        return idpresupuesto;
    }

    public void setIdpresupuesto(Presupuesto idpresupuesto) {
        this.idpresupuesto = idpresupuesto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgasdoce != null ? idgasdoce.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gastospersonal)) {
            return false;
        }
        Gastospersonal other = (Gastospersonal) object;
        if ((this.idgasdoce == null && other.idgasdoce != null) || (this.idgasdoce != null && !this.idgasdoce.equals(other.idgasdoce))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Gastospersonal[ idgasdoce=" + idgasdoce + " ]";
    }
    
}
