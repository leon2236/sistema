/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "INFORAVAN", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inforavan.findAll", query = "SELECT i FROM Inforavan i"),
    @NamedQuery(name = "Inforavan.findByIdinfava", query = "SELECT i FROM Inforavan i WHERE i.idinfava = :idinfava"),
    @NamedQuery(name = "Inforavan.findByFechapresentacion", query = "SELECT i FROM Inforavan i WHERE i.fechapresentacion = :fechapresentacion"),
    @NamedQuery(name = "Inforavan.findByNumdeinforme", query = "SELECT i FROM Inforavan i WHERE i.numdeinforme = :numdeinforme"),
    @NamedQuery(name = "Inforavan.findByContinua", query = "SELECT i FROM Inforavan i WHERE i.continua = :continua"),
    @NamedQuery(name = "Inforavan.findByObjetivos", query = "SELECT i FROM Inforavan i WHERE i.objetivos = :objetivos"),
    @NamedQuery(name = "Inforavan.findByActividades", query = "SELECT i FROM Inforavan i WHERE i.actividades = :actividades"),
    @NamedQuery(name = "Inforavan.findByCronograma", query = "SELECT i FROM Inforavan i WHERE i.cronograma = :cronograma"),
    @NamedQuery(name = "Inforavan.findByMetas", query = "SELECT i FROM Inforavan i WHERE i.metas = :metas")})
public class Inforavan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDINFAVA")
    private String idinfava;
    @Column(name = "FECHAPRESENTACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechapresentacion;
    @Column(name = "NUMDEINFORME")
    private BigInteger numdeinforme;
    @Size(max = 10)
    @Column(name = "CONTINUA")
    private String continua;
    @Column(name = "OBJETIVOS")
    private Character objetivos;
    @Column(name = "ACTIVIDADES")
    private Character actividades;
    @Column(name = "CRONOGRAMA")
    private Character cronograma;
    @Column(name = "METAS")
    private Character metas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinformeavance")
    private Collection<Pregunta> preguntaCollection;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinfoavan")
    private Collection<Infoauxi> infoauxiCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinfoavan")
    private Collection<Activimetas> activimetasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinfoavan")
    private Collection<Infofinan> infofinanCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinfoavan")
    private Collection<Resulparcial> resulparcialCollection;

    public Inforavan() {
    }

    public Inforavan(String idinfava) {
        this.idinfava = idinfava;
    }

    public String getIdinfava() {
        return idinfava;
    }

    public void setIdinfava(String idinfava) {
        this.idinfava = idinfava;
    }

    public Date getFechapresentacion() {
        return fechapresentacion;
    }

    public void setFechapresentacion(Date fechapresentacion) {
        this.fechapresentacion = fechapresentacion;
    }

    public BigInteger getNumdeinforme() {
        return numdeinforme;
    }

    public void setNumdeinforme(BigInteger numdeinforme) {
        this.numdeinforme = numdeinforme;
    }

    public String getContinua() {
        return continua;
    }

    public void setContinua(String continua) {
        this.continua = continua;
    }

    public Character getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(Character objetivos) {
        this.objetivos = objetivos;
    }

    public Character getActividades() {
        return actividades;
    }

    public void setActividades(Character actividades) {
        this.actividades = actividades;
    }

    public Character getCronograma() {
        return cronograma;
    }

    public void setCronograma(Character cronograma) {
        this.cronograma = cronograma;
    }

    public Character getMetas() {
        return metas;
    }

    public void setMetas(Character metas) {
        this.metas = metas;
    }

    @XmlTransient
    public Collection<Pregunta> getPreguntaCollection() {
        return preguntaCollection;
    }

    public void setPreguntaCollection(Collection<Pregunta> preguntaCollection) {
        this.preguntaCollection = preguntaCollection;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @XmlTransient
    public Collection<Infoauxi> getInfoauxiCollection() {
        return infoauxiCollection;
    }

    public void setInfoauxiCollection(Collection<Infoauxi> infoauxiCollection) {
        this.infoauxiCollection = infoauxiCollection;
    }

    @XmlTransient
    public Collection<Activimetas> getActivimetasCollection() {
        return activimetasCollection;
    }

    public void setActivimetasCollection(Collection<Activimetas> activimetasCollection) {
        this.activimetasCollection = activimetasCollection;
    }

    @XmlTransient
    public Collection<Infofinan> getInfofinanCollection() {
        return infofinanCollection;
    }

    public void setInfofinanCollection(Collection<Infofinan> infofinanCollection) {
        this.infofinanCollection = infofinanCollection;
    }

    @XmlTransient
    public Collection<Resulparcial> getResulparcialCollection() {
        return resulparcialCollection;
    }

    public void setResulparcialCollection(Collection<Resulparcial> resulparcialCollection) {
        this.resulparcialCollection = resulparcialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinfava != null ? idinfava.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inforavan)) {
            return false;
        }
        Inforavan other = (Inforavan) object;
        if ((this.idinfava == null && other.idinfava != null) || (this.idinfava != null && !this.idinfava.equals(other.idinfava))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Inforavan[ idinfava=" + idinfava + " ]";
    }
    
}
