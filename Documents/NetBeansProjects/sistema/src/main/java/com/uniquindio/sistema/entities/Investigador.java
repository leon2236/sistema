/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "INVESTIGADOR", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Investigador.findAll", query = "SELECT i FROM Investigador i"),
    @NamedQuery(name = "Investigador.findByIdinvestigador", query = "SELECT i FROM Investigador i WHERE i.idinvestigador = :idinvestigador"),
    @NamedQuery(name = "Investigador.findByNombre", query = "SELECT i FROM Investigador i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Investigador.findByApellidos", query = "SELECT i FROM Investigador i WHERE i.apellidos = :apellidos"),
    @NamedQuery(name = "Investigador.findByEdad", query = "SELECT i FROM Investigador i WHERE i.edad = :edad"),
    @NamedQuery(name = "Investigador.findByTelefonofijo", query = "SELECT i FROM Investigador i WHERE i.telefonofijo = :telefonofijo"),
    @NamedQuery(name = "Investigador.findByTelefonocelular", query = "SELECT i FROM Investigador i WHERE i.telefonocelular = :telefonocelular"),
    @NamedQuery(name = "Investigador.findByCorreo", query = "SELECT i FROM Investigador i WHERE i.correo = :correo"),
    @NamedQuery(name = "Investigador.findByDireccion", query = "SELECT i FROM Investigador i WHERE i.direccion = :direccion"),
    @NamedQuery(name = "Investigador.findByPrincipal", query = "SELECT i FROM Investigador i WHERE i.principal = :principal"),
    @NamedQuery(name = "Investigador.findByRol", query = "SELECT i FROM Investigador i WHERE i.rol = :rol"),
    @NamedQuery(name = "Investigador.findByTipovinculacion", query = "SELECT i FROM Investigador i WHERE i.tipovinculacion = :tipovinculacion"),
    @NamedQuery(name = "Investigador.findByCurriculumruta", query = "SELECT i FROM Investigador i WHERE i.curriculumruta = :curriculumruta")})
public class Investigador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDINVESTIGADOR")
    private String idinvestigador;
    @Size(max = 50)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 50)
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Column(name = "EDAD")
    private BigInteger edad;
    @Size(max = 20)
    @Column(name = "TELEFONOFIJO")
    private String telefonofijo;
    @Size(max = 20)
    @Column(name = "TELEFONOCELULAR")
    private String telefonocelular;
    @Size(max = 50)
    @Column(name = "CORREO")
    private String correo;
    @Size(max = 50)
    @Column(name = "DIRECCION")
    private String direccion;
    @Column(name = "PRINCIPAL")
    private Character principal;
    @Size(max = 20)
    @Column(name = "ROL")
    private String rol;
    @Size(max = 20)
    @Column(name = "TIPOVINCULACION")
    private String tipovinculacion;
    @Size(max = 200)
    @Column(name = "CURRICULUMRUTA")
    private String curriculumruta;
    @ManyToMany(mappedBy = "investigadorCollection")
    private Collection<Proyecto> proyectoCollection;
    @JoinColumn(name = "IDGRUPOINV", referencedColumnName = "IDGINVES")
    @ManyToOne(optional = false)
    private Grupoinvestigacion idgrupoinv;
    @JoinColumn(name = "IDPROGRAMA", referencedColumnName = "IDPROGRAMA")
    @ManyToOne(optional = false)
    private Programa idprograma;
    @OneToMany(mappedBy = "idinvestigador")
    private Collection<Usuario> usuarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinvestigador")
    private Collection<Titulo> tituloCollection;

    public Investigador() {
    }

    public Investigador(String idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    public String getIdinvestigador() {
        return idinvestigador;
    }

    public void setIdinvestigador(String idinvestigador) {
        this.idinvestigador = idinvestigador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public BigInteger getEdad() {
        return edad;
    }

    public void setEdad(BigInteger edad) {
        this.edad = edad;
    }

    public String getTelefonofijo() {
        return telefonofijo;
    }

    public void setTelefonofijo(String telefonofijo) {
        this.telefonofijo = telefonofijo;
    }

    public String getTelefonocelular() {
        return telefonocelular;
    }

    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getTipovinculacion() {
        return tipovinculacion;
    }

    public void setTipovinculacion(String tipovinculacion) {
        this.tipovinculacion = tipovinculacion;
    }

    public String getCurriculumruta() {
        return curriculumruta;
    }

    public void setCurriculumruta(String curriculumruta) {
        this.curriculumruta = curriculumruta;
    }

    @XmlTransient
    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    public Grupoinvestigacion getIdgrupoinv() {
        return idgrupoinv;
    }

    public void setIdgrupoinv(Grupoinvestigacion idgrupoinv) {
        this.idgrupoinv = idgrupoinv;
    }

    public Programa getIdprograma() {
        return idprograma;
    }

    public void setIdprograma(Programa idprograma) {
        this.idprograma = idprograma;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @XmlTransient
    public Collection<Titulo> getTituloCollection() {
        return tituloCollection;
    }

    public void setTituloCollection(Collection<Titulo> tituloCollection) {
        this.tituloCollection = tituloCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinvestigador != null ? idinvestigador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Investigador)) {
            return false;
        }
        Investigador other = (Investigador) object;
        if ((this.idinvestigador == null && other.idinvestigador != null) || (this.idinvestigador != null && !this.idinvestigador.equals(other.idinvestigador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Investigador[ idinvestigador=" + idinvestigador + " ]";
    }
    
}
