/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "OBJETIVO", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Objetivo.findAll", query = "SELECT o FROM Objetivo o"),
    @NamedQuery(name = "Objetivo.findByIdopbjetivo", query = "SELECT o FROM Objetivo o WHERE o.idopbjetivo = :idopbjetivo"),
    @NamedQuery(name = "Objetivo.findByTipo", query = "SELECT o FROM Objetivo o WHERE o.tipo = :tipo"),
    @NamedQuery(name = "Objetivo.findByDescripcion", query = "SELECT o FROM Objetivo o WHERE o.descripcion = :descripcion")})
public class Objetivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDOPBJETIVO")
    private String idopbjetivo;
    @Size(max = 20)
    @Column(name = "TIPO")
    private String tipo;
    @Size(max = 1000)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne(optional = false)
    private Proyecto idproyecto;

    public Objetivo() {
    }

    public Objetivo(String idopbjetivo) {
        this.idopbjetivo = idopbjetivo;
    }

    public String getIdopbjetivo() {
        return idopbjetivo;
    }

    public void setIdopbjetivo(String idopbjetivo) {
        this.idopbjetivo = idopbjetivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idopbjetivo != null ? idopbjetivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objetivo)) {
            return false;
        }
        Objetivo other = (Objetivo) object;
        if ((this.idopbjetivo == null && other.idopbjetivo != null) || (this.idopbjetivo != null && !this.idopbjetivo.equals(other.idopbjetivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Objetivo[ idopbjetivo=" + idopbjetivo + " ]";
    }
    
}
