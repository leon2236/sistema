/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "OFICIO", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Oficio.findAll", query = "SELECT o FROM Oficio o"),
    @NamedQuery(name = "Oficio.findByIdoficio", query = "SELECT o FROM Oficio o WHERE o.idoficio = :idoficio"),
    @NamedQuery(name = "Oficio.findByUrl", query = "SELECT o FROM Oficio o WHERE o.url = :url")})
public class Oficio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDOFICIO")
    private String idoficio;
    @Size(max = 100)
    @Column(name = "URL")
    private String url;
    @JoinColumn(name = "IDPROYECTO", referencedColumnName = "IDPROYECTO")
    @ManyToOne
    private Proyecto idproyecto;

    public Oficio() {
    }

    public Oficio(String idoficio) {
        this.idoficio = idoficio;
    }

    public String getIdoficio() {
        return idoficio;
    }

    public void setIdoficio(String idoficio) {
        this.idoficio = idoficio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Proyecto getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(Proyecto idproyecto) {
        this.idproyecto = idproyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idoficio != null ? idoficio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oficio)) {
            return false;
        }
        Oficio other = (Oficio) object;
        if ((this.idoficio == null && other.idoficio != null) || (this.idoficio != null && !this.idoficio.equals(other.idoficio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Oficio[ idoficio=" + idoficio + " ]";
    }
    
}
