/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "ORGFINANCIEROS", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orgfinancieros.findAll", query = "SELECT o FROM Orgfinancieros o"),
    @NamedQuery(name = "Orgfinancieros.findByIdorga", query = "SELECT o FROM Orgfinancieros o WHERE o.idorga = :idorga"),
    @NamedQuery(name = "Orgfinancieros.findByNombre", query = "SELECT o FROM Orgfinancieros o WHERE o.nombre = :nombre"),
    @NamedQuery(name = "Orgfinancieros.findByEfectivo", query = "SELECT o FROM Orgfinancieros o WHERE o.efectivo = :efectivo"),
    @NamedQuery(name = "Orgfinancieros.findByRecurrente", query = "SELECT o FROM Orgfinancieros o WHERE o.recurrente = :recurrente"),
    @NamedQuery(name = "Orgfinancieros.findByPorcentaje", query = "SELECT o FROM Orgfinancieros o WHERE o.porcentaje = :porcentaje")})
public class Orgfinancieros implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDORGA")
    private String idorga;
    @Size(max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "EFECTIVO")
    private BigInteger efectivo;
    @Size(max = 10)
    @Column(name = "RECURRENTE")
    private String recurrente;
    @Column(name = "PORCENTAJE")
    private BigInteger porcentaje;
    @ManyToMany(mappedBy = "orgfinancierosCollection")
    private Collection<Actainicio> actainicioCollection;

    public Orgfinancieros() {
    }

    public Orgfinancieros(String idorga) {
        this.idorga = idorga;
    }

    public String getIdorga() {
        return idorga;
    }

    public void setIdorga(String idorga) {
        this.idorga = idorga;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(BigInteger efectivo) {
        this.efectivo = efectivo;
    }

    public String getRecurrente() {
        return recurrente;
    }

    public void setRecurrente(String recurrente) {
        this.recurrente = recurrente;
    }

    public BigInteger getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigInteger porcentaje) {
        this.porcentaje = porcentaje;
    }

    @XmlTransient
    public Collection<Actainicio> getActainicioCollection() {
        return actainicioCollection;
    }

    public void setActainicioCollection(Collection<Actainicio> actainicioCollection) {
        this.actainicioCollection = actainicioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idorga != null ? idorga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orgfinancieros)) {
            return false;
        }
        Orgfinancieros other = (Orgfinancieros) object;
        if ((this.idorga == null && other.idorga != null) || (this.idorga != null && !this.idorga.equals(other.idorga))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Orgfinancieros[ idorga=" + idorga + " ]";
    }
    
}
