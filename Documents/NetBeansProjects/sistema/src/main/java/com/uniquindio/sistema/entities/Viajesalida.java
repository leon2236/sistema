/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leon
 */
@Entity
@Table(name = "VIAJESALIDA", catalog = "", schema = "SIVIUQ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Viajesalida.findAll", query = "SELECT v FROM Viajesalida v"),
    @NamedQuery(name = "Viajesalida.findByIdviaje", query = "SELECT v FROM Viajesalida v WHERE v.idviaje = :idviaje"),
    @NamedQuery(name = "Viajesalida.findByLugar", query = "SELECT v FROM Viajesalida v WHERE v.lugar = :lugar"),
    @NamedQuery(name = "Viajesalida.findByJustificacion", query = "SELECT v FROM Viajesalida v WHERE v.justificacion = :justificacion"),
    @NamedQuery(name = "Viajesalida.findByCostopasaje", query = "SELECT v FROM Viajesalida v WHERE v.costopasaje = :costopasaje"),
    @NamedQuery(name = "Viajesalida.findByCostoestadia", query = "SELECT v FROM Viajesalida v WHERE v.costoestadia = :costoestadia"),
    @NamedQuery(name = "Viajesalida.findByNumerodias", query = "SELECT v FROM Viajesalida v WHERE v.numerodias = :numerodias"),
    @NamedQuery(name = "Viajesalida.findByValortotal", query = "SELECT v FROM Viajesalida v WHERE v.valortotal = :valortotal"),
    @NamedQuery(name = "Viajesalida.findByViaje", query = "SELECT v FROM Viajesalida v WHERE v.viaje = :viaje"),
    @NamedQuery(name = "Viajesalida.findBySalidacampo", query = "SELECT v FROM Viajesalida v WHERE v.salidacampo = :salidacampo"),
    @NamedQuery(name = "Viajesalida.findByRubro1", query = "SELECT v FROM Viajesalida v WHERE v.rubro1 = :rubro1"),
    @NamedQuery(name = "Viajesalida.findByRubro2", query = "SELECT v FROM Viajesalida v WHERE v.rubro2 = :rubro2"),
    @NamedQuery(name = "Viajesalida.findByRubro3", query = "SELECT v FROM Viajesalida v WHERE v.rubro3 = :rubro3"),
    @NamedQuery(name = "Viajesalida.findByRubro4", query = "SELECT v FROM Viajesalida v WHERE v.rubro4 = :rubro4")})
public class Viajesalida implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IDVIAJE")
    private String idviaje;
    @Size(max = 1000)
    @Column(name = "LUGAR")
    private String lugar;
    @Size(max = 1000)
    @Column(name = "JUSTIFICACION")
    private String justificacion;
    @Column(name = "COSTOPASAJE")
    private BigInteger costopasaje;
    @Column(name = "COSTOESTADIA")
    private BigInteger costoestadia;
    @Column(name = "NUMERODIAS")
    private BigInteger numerodias;
    @Column(name = "VALORTOTAL")
    private BigInteger valortotal;
    @Column(name = "VIAJE")
    private Character viaje;
    @Column(name = "SALIDACAMPO")
    private Character salidacampo;
    @Column(name = "RUBRO1")
    private BigInteger rubro1;
    @Column(name = "RUBRO2")
    private BigInteger rubro2;
    @Column(name = "RUBRO3")
    private BigInteger rubro3;
    @Column(name = "RUBRO4")
    private BigInteger rubro4;
    @JoinColumn(name = "IDCUADROPRESUPUESTO", referencedColumnName = "IDPRESUPUESTO")
    @ManyToOne
    private Presupuesto idcuadropresupuesto;

    public Viajesalida() {
    }

    public Viajesalida(String idviaje) {
        this.idviaje = idviaje;
    }

    public String getIdviaje() {
        return idviaje;
    }

    public void setIdviaje(String idviaje) {
        this.idviaje = idviaje;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public BigInteger getCostopasaje() {
        return costopasaje;
    }

    public void setCostopasaje(BigInteger costopasaje) {
        this.costopasaje = costopasaje;
    }

    public BigInteger getCostoestadia() {
        return costoestadia;
    }

    public void setCostoestadia(BigInteger costoestadia) {
        this.costoestadia = costoestadia;
    }

    public BigInteger getNumerodias() {
        return numerodias;
    }

    public void setNumerodias(BigInteger numerodias) {
        this.numerodias = numerodias;
    }

    public BigInteger getValortotal() {
        return valortotal;
    }

    public void setValortotal(BigInteger valortotal) {
        this.valortotal = valortotal;
    }

    public Character getViaje() {
        return viaje;
    }

    public void setViaje(Character viaje) {
        this.viaje = viaje;
    }

    public Character getSalidacampo() {
        return salidacampo;
    }

    public void setSalidacampo(Character salidacampo) {
        this.salidacampo = salidacampo;
    }

    public BigInteger getRubro1() {
        return rubro1;
    }

    public void setRubro1(BigInteger rubro1) {
        this.rubro1 = rubro1;
    }

    public BigInteger getRubro2() {
        return rubro2;
    }

    public void setRubro2(BigInteger rubro2) {
        this.rubro2 = rubro2;
    }

    public BigInteger getRubro3() {
        return rubro3;
    }

    public void setRubro3(BigInteger rubro3) {
        this.rubro3 = rubro3;
    }

    public BigInteger getRubro4() {
        return rubro4;
    }

    public void setRubro4(BigInteger rubro4) {
        this.rubro4 = rubro4;
    }

    public Presupuesto getIdcuadropresupuesto() {
        return idcuadropresupuesto;
    }

    public void setIdcuadropresupuesto(Presupuesto idcuadropresupuesto) {
        this.idcuadropresupuesto = idcuadropresupuesto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idviaje != null ? idviaje.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viajesalida)) {
            return false;
        }
        Viajesalida other = (Viajesalida) object;
        if ((this.idviaje == null && other.idviaje != null) || (this.idviaje != null && !this.idviaje.equals(other.idviaje))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uniquindio.sistema.entities.Viajesalida[ idviaje=" + idviaje + " ]";
    }
    
}
