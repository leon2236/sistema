/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.negocio;

import com.uniquindio.sistema.bean.BeanLogin;
import com.uniquindio.sistema.entities.Usuario;
import com.uniquindio.sistema.exception.SVException;

/**
 *
 * @author Juliancho
 */
public interface NegocioLogin {
   public void validarUsuario(BeanLogin obj) throws SVException; 
}
