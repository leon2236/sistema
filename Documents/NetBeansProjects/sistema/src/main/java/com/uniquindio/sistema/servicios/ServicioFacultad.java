/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.servicios;

import com.uniquindio.sistema.bean.BeanFacultad;
import com.uniquindio.sistema.exception.SVException;
import com.uniquindio.sistema.negocio.NegocioFacultad;

/**
 *
 * @author Juliancho
 */
public class ServicioFacultad implements IfaceFacultad{
    private NegocioFacultad daoFacultad;
   
    @Override
    public void validarFacultad(BeanFacultad bean) throws  SVException{
      getDaoFacultad().validarFacultad(bean);
    }
    
    
    public NegocioFacultad getDaoFacultad(){
        return daoFacultad;
    } 
}
