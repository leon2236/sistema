/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uniquindio.sistema.servicios;

import com.uniquindio.sistema.bean.BeanLogin;
import com.uniquindio.sistema.entities.Usuario;
import com.uniquindio.sistema.exception.SVException;
import com.uniquindio.sistema.negocio.NegocioLogin;

/**
 *
 * @author Juliancho
 */
public class ServicioLogin implements IfaceLogin {
   private NegocioLogin daoLogin;
   
    @Override
    public void validarUsuario(BeanLogin bean) throws  SVException{
        getDaoLogin().validarUsuario(bean);
    }
    
    
    public NegocioLogin getDaoLogin(){
        return daoLogin;
    }
    
    public void setDaoLogin(NegocioLogin daoLogin){
        this.daoLogin=daoLogin;
    }
}
